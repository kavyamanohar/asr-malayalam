This is a [kaldi](https://kaldi-asr.org/) based recipie for Malayalam speech recognition. You need a working Kaldi directory to run this script.

Details on how to run this script and the working is described here.

To install Kaldi, see the documentation [here](https://kaldi-asr.org/doc/install.html)

The source code of `/asr_malayalam` has to be placed in the `/egs` directory of Kaldi installation directory.

## USAGE

`./run_gmm.sh ./inputdirectory`

input directory has the following structure:
```
├── language -> symlink/to/language/model/source ( lm_train.txt, lexicon.txt)
├── test
│   └── corpus1
│       ├── audio -> symlink/to/audio/files_directory (utt1.wav, utt2.wav)
│       └── metadata.tsv -> symlink/to/audio/files/metadata (metadata.tsv)
└── train
    ├── corpus2
    |      ├── audio -> symlink/to/audio/files_directory (utt1.wav, utt2.wav)
    |      └── metadata.tsv -> symlink/to/audio/files/metadata (metadata.tsv)
    └── corpus3
        ├── audio -> symlink/to/audio/files_directory (utt1.wav, utt2.wav)
        └── metadata.tsv -> symlink/to/audio/files/metadata (metadata.tsv)
```
metadata.tsv is a tab separated values of utterence_id, speaker_id, file_name in audio folder, transcript in Malayalam script.

This script performs the following tasks:

- Create bigram word level LM grammar from text files
- Uses predefined phonetic lexicon
- Extracts MFCC features(13 cepstral bins) after converting all sampling rates to 16kHz
- Trains mono, tri, tri_lda, tri_sat acoustic models (AM) is that order, using alignments from previous stage.
- Compiles the LM grammar, phonetic lexicon and acoustic models to create HCLG.fst graph
- Test each acoustic model and stores the decoding results



`./run_chain.sh`

This script performs the following tasks:

- Runs ivector training, extracts 30 dimensional ivectors. It uses high resolution MFCC (40 cepstral bins)
- Use alignments from the best trigram model (Currently hardcoded to tri_lda) to start nnet3 AM training
- nnet3 AM trained on CUDA compiled GPU (single Tesla T4)
- Checks phone compatibility of LM
- Make graph with new AM model
- Test and save the results

## TRAINING DATA

### Training data for acoustic modeling:

1. [Indic TTS Malayalam corpus](https://www.kaggle.com/kavyamanohar/indic-tts-malayalam-speech-corpus)

    Training data used is studio recorded speech by:
    - 2 speakers
    - 8601 utterances
    - 13 hour 58 minutes 20 seconds
    - 48 kHz sampling rate

2. [OpenSLR Malayalam](http://openslr.org/resources/63/) - Split into train and test sets. (See datasplits directory for details)

    Training data used is studio recorded speech by:
    - 37 speakers
    - 3446 utterances
    - 4 hour 47 minutes 47 seconds
    - 48 kHz sampling rate

### Language Model:

Language model is trained on sentence corpora that comprises of:

1. All training audio transcripts (7924 sentences)
2. [SMC Text corpus](https://gitlab.com/smc/corpus) (205k sentences)

The text was unicode normalized after removing foreign scripts, numerals, punctuations etc.

(All test transcripts are explicitly removed from these sentences before LM training)
### Phonetic Lexicon

Phonetic Lexicon created using Mlphon using

1. Words in training audio transcripts
2. Words of atleast 4 occurence in LM training sentences
### TEST FILES

The models are tested on three data sets:

1. [Festvox IIITH Malayalam database](http://www.festvox.org/databases/iiit_voices/)

    Test data used is studio recorded speech by:
    - 1 speaker
    - 1000 utterances
    - 1 hour 38 minutes
    - 16 kHz sampling rate

2. [OpenSLR Malayalam](http://openslr.org/resources/63/) - Split into train and test sets. (See datasplits directory for details)

    Test data used is studio recorded speech by:
    - 7 speaker
    - 48 minutes
    - 48 kHz sampling rate
